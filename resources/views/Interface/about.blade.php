@extends('layouts.app')
  @section('content')
    <div class="m-20">
    <h1 class="text-4xl">Why did we made this A.I or Admin Interface?</h1> 
    <p class="m-5 text-xl">This application is made to show you an Admin Interface would look like and how the functionality to it works, so in this documentation, we give will you some insights on how Admin Interface works:</p>
    <h1 class="text-2xl">Dashboard</h1>
    <p class="m-5">Dashboard is an overview of overall results of every System that a website can have and 
    this is much more efficient because dashboard is the one that will give all the important details of your 
    System and this is very crucial so that you are monitoring would be much more faster to navigate to your system
    in any way possible.</p> 
    <h1 class="text-2xl">Users</h1>
    <p class="m-5">Users is a Users List is to Show and edit's user's important details on website making it much more easier to know about user's details.</p> 
    <h1 class="text-2xl">Profiles</h1>
    <p class="m-5">Profiles is a Profile Management System, unlike User's List, Profile Management System takes about every information that a person could have to know them better, so you can edit user's profile credential by updatng its every input fields in the way you need and wants.</p> 
    <h1 class="text-2xl">Library</h1>
    <p class="m-5">Library is a Library Management System to store or publish and delete it's E-Book File, this system aims to give user's knowledge about every published book that they could choose from and user's will have the advantage to get this by downloading the E-Book File they want.</p> 
    </div>
  @endsection